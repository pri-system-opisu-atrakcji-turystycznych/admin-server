FROM tomcat
USER root
COPY tomcat-users.xml /usr/local/tomcat/conf/
COPY context.xml /usr/local/tomcat/webapps/manager/META-INF/
COPY /target/artifact-1.0-SNAPSHOT.war /usr/local/tomcat/webapps/adminserver.war
CMD ["catalina.sh","run"]
