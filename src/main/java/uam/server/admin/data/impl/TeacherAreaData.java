package uam.server.admin.data.impl;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uam.server.admin.data.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TeacherAreaData implements Data {

    private Long id;
    @NotEmpty
    @Size(min = 1)
    private String name;
    @NotEmpty
    @Size(min = 4)
    private String url;
    @NotEmpty
    @Size(min = 4, max = 4)
    private String teacherCode;
    private String mobile;
    private String web;

}
