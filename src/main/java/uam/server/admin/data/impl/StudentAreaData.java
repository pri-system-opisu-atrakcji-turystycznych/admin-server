package uam.server.admin.data.impl;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uam.server.admin.data.Data;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentAreaData implements Data {

    private Long id;
    private String name;
    private String url;
    private String mobile;
    private String web;

}
