package uam.server.admin.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uam.server.admin.constant.ModelConstants.Role;
import uam.server.admin.constant.ModelConstants.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = User.TABLE)
public class UserModel extends Model implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = User.ID)
    private Long id;

    @Column(name = User.EMAIL)
    @Email(message = "*Please provide a valid Email")
    @NotEmpty(message = "*Please provide an email")
    private String email;

    @Column(name = User.PASSWORD)
    @NotEmpty
    private String password;

    @Column(name = User.ACTIVE)
    private Long active;

    @ManyToMany
    @JoinTable(name = User.USER_ROLES,
            joinColumns = @JoinColumn(name = User.TABLE),
            inverseJoinColumns = @JoinColumn(name = Role.TABLE))
    private Set<RoleModel> roles;

}
