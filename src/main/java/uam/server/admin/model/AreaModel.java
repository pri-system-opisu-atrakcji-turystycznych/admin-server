package uam.server.admin.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uam.server.admin.constant.ModelConstants.Area;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = Area.TABLE)
public class AreaModel extends Model implements Serializable {

    @Id
    @Column(name = Area.ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = Area.NAME)
    private String name;

    @Column(name = Area.URL)
    private String url;

    @Column(name = Area.TEACHER_CODE)
    @Size(min = 4, max = 4)
    private String teacherCode;

    @Column(name = Area.MOBILE)
    private Boolean mobile;

    @Column(name = Area.WEB)
    private Boolean web;

}
