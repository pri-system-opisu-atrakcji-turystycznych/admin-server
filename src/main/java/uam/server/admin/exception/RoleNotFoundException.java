package uam.server.admin.exception;

public class RoleNotFoundException extends RuntimeException {
    public RoleNotFoundException(final String name) {
        super("Role not found for name: " + name);
    }

}
