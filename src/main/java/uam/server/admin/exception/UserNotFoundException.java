package uam.server.admin.exception;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(final String email) {
        super("User not found for email: " + email);
    }

}
