package uam.server.admin.exception;

public class AreaNotFoundException extends RuntimeException {
    public AreaNotFoundException(final String identifier) {
        super("Area not found for name/url: " + identifier);
    }

    public AreaNotFoundException(final Long id) {
        super("Area not found for id:  " + id);
    }

}
