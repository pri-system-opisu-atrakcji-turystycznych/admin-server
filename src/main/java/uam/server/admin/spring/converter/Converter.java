package uam.server.admin.spring.converter;

import uam.server.admin.data.Data;
import uam.server.admin.model.Model;

public interface Converter<D extends Data, M extends Model> {

    D convert(M model);

    M inverseConvert(D data);
}
