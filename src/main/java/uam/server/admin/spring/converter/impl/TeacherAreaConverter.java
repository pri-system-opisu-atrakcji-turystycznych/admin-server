package uam.server.admin.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.admin.data.impl.TeacherAreaData;
import uam.server.admin.model.AreaModel;
import uam.server.admin.spring.converter.Converter;

@Service
public class TeacherAreaConverter implements Converter<TeacherAreaData, AreaModel> {
    @Override
    public TeacherAreaData convert(final AreaModel model) {
        return TeacherAreaData.builder()
                .id(model.getId())
                .name(model.getName())
                .url(model.getUrl())
                .teacherCode(model.getTeacherCode())
                .mobile(String.valueOf(model.getMobile()))
                .web(String.valueOf(model.getWeb()))
                .build();
    }

    @Override
    public AreaModel inverseConvert(final TeacherAreaData data) {
        return AreaModel.builder()
                .id(data.getId())
                .name(data.getName())
                .url(data.getUrl())
                .teacherCode(data.getTeacherCode())
                .mobile(Boolean.valueOf(data.getMobile()))
                .web(Boolean.valueOf(data.getWeb()))
                .build();
    }
}
