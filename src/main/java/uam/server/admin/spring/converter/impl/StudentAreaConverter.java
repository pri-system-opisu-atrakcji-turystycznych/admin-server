package uam.server.admin.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.admin.data.impl.StudentAreaData;
import uam.server.admin.model.AreaModel;
import uam.server.admin.spring.converter.Converter;

@Service
public class StudentAreaConverter implements Converter<StudentAreaData, AreaModel> {
    @Override
    public StudentAreaData convert(final AreaModel model) {
        return StudentAreaData.builder()
                .id(model.getId())
                .name(model.getName())
                .url(model.getUrl())
                .mobile(String.valueOf(model.getMobile()))
                .web(String.valueOf(model.getWeb()))
                .build();
    }

    @Override
    public AreaModel inverseConvert(final StudentAreaData data) {
        return AreaModel.builder()
                .id(data.getId())
                .name(data.getName())
                .url(data.getUrl())
                .mobile(Boolean.valueOf(data.getMobile()))
                .web(Boolean.valueOf(data.getWeb()))
                .build();
    }
}
