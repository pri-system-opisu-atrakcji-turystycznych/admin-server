package uam.server.admin.spring.converter.impl;

import org.springframework.stereotype.Service;
import uam.server.admin.data.impl.UserData;
import uam.server.admin.model.UserModel;
import uam.server.admin.spring.converter.Converter;

@Service
public class UserConverter implements Converter<UserData, UserModel> {
    @Override
    public UserData convert(UserModel model) {
        return UserData.builder()
                .email(model.getEmail())
                .build();
    }

    @Override
    public UserModel inverseConvert(UserData data) {
        return UserModel.builder()
                .email(data.getEmail())
                .build();
    }
}
