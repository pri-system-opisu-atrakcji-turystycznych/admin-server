package uam.server.admin.spring.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import uam.server.admin.constant.WebConstants.Mapping;

@CrossOrigin
@RestController
@RequestMapping(Mapping.SERVER)
public class ServerController {

    @RequestMapping(value = Mapping.STATUS, method = RequestMethod.GET)
    public String getStatus() {
        return "Work!";
    }

}
