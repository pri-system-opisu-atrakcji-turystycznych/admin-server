package uam.server.admin.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uam.server.admin.constant.WebConstants.Mapping;
import uam.server.admin.constant.WebConstants.Views;

@Controller
@RequestMapping(value = {Mapping.ROOT, Mapping.LOGIN})
public class LoginPageController {

    @GetMapping
    public String login() {
        return Views.LOGIN_PAGE;
    }

}
