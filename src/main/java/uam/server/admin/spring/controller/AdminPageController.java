package uam.server.admin.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uam.server.admin.constant.WebConstants.Mapping;
import uam.server.admin.constant.WebConstants.Views;
import uam.server.admin.data.impl.TeacherAreaData;
import uam.server.admin.data.impl.UserData;
import uam.server.admin.spring.facade.TeacherAreaFacade;
import uam.server.admin.spring.facade.UserFacade;

import javax.annotation.Resource;
import javax.validation.Valid;

import static uam.server.admin.constant.WebConstants.Attribute.AREAS;
import static uam.server.admin.constant.WebConstants.Attribute.USERS;

@Controller
@RequestMapping(Mapping.ADMIN)
public class AdminPageController {

    @Resource
    private TeacherAreaFacade teacherAreaFacade;
    @Resource
    private UserFacade userFacade;

    @RequestMapping(Mapping.HOME)
    public String get(final Model model) {
        return getPopulateCommonAttributes(model);
    }

    @PostMapping(Mapping.AREA_ADD)
    public String addArea(@Valid final TeacherAreaData area, final BindingResult bindingResult, final Model model) {
        if (!bindingResult.hasErrors()) {
            teacherAreaFacade.addArea(area);
        }
        return getPopulateCommonAttributes(model);
    }

    @PostMapping(Mapping.AREA_MODIFY)
    public String modifyArea(@Valid final TeacherAreaData area, final BindingResult bindingResult, final Model model) {
        if (!bindingResult.hasErrors()) {
            teacherAreaFacade.modifyArea(area);
        }
        return getPopulateCommonAttributes(model);
    }

    @PostMapping(Mapping.AREA_DELETE)
    public String deleteArea(final Long id, final Model model) {
        teacherAreaFacade.deleteArea(id);
        return getPopulateCommonAttributes(model);
    }

    @PostMapping(Mapping.USER_ADD)
    public String addUser(@Valid final UserData user,
                          final BindingResult bindingResult,
                          final Model model) {
        if (!bindingResult.hasErrors()) {
            userFacade.addUser(user);
        }
        return getPopulateCommonAttributes(model);
    }

    @PostMapping(Mapping.USER_DELETE)
    public String deleteUser(final String email, final Model model) {
        userFacade.deleteUser(email);
        return getPopulateCommonAttributes(model);
    }

    private String getPopulateCommonAttributes(final Model model) {
        model.addAttribute(USERS, userFacade.getAll());
        model.addAttribute(AREAS, teacherAreaFacade.getAllTeacherArea());
        return Views.ADMIN_PAGE;
    }

}
