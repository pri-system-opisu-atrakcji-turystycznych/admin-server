package uam.server.admin.spring.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import uam.server.admin.constant.WebConstants.Mapping;
import uam.server.admin.data.impl.StudentAreaData;
import uam.server.admin.data.impl.TeacherAreaData;
import uam.server.admin.model.AreaModel;
import uam.server.admin.spring.facade.StudentAreaFacade;
import uam.server.admin.spring.facade.TeacherAreaFacade;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@RequestMapping(Mapping.AREAS)
public class AreaController {

    @Resource
    private TeacherAreaFacade teacherAreaFacade;
    @Resource
    private StudentAreaFacade studentAreaFacade;

    @GetMapping(value = Mapping.STUDENT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<StudentAreaData> getAllStudentArea() {
        return studentAreaFacade.getAllStudentArea();
    }

    @GetMapping(value = Mapping.TEACHER, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<TeacherAreaData> getAllTeacherArea() {
        return teacherAreaFacade.getAllTeacherArea();
    }

    @GetMapping(value = Mapping.MOBILE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Iterable<TeacherAreaData> getMobileAreas() {
        return teacherAreaFacade.getAreaByMobileTrue();
    }

    @GetMapping(value = Mapping.WEB, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Iterable<StudentAreaData> getWebAreas() {
        return studentAreaFacade.getAreaByWebTrue();
    }

    @GetMapping(value = Mapping.NAME, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AreaModel getAreaByName(@RequestParam("name") String name) {
        return teacherAreaFacade.getTeacherAreaByName(name);
    }

    @GetMapping(value = Mapping.URL, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AreaModel getAreaByUrl(@RequestParam("url") String url) {
        return teacherAreaFacade.getTeacherAreaByUrl(url);
    }

}