package uam.server.admin.spring.facade;

import uam.server.admin.data.impl.UserData;

import java.util.List;

public interface UserFacade {

    List<UserData> getAll();

    void addUser(final UserData user);

    void deleteUser(final String email);
}
