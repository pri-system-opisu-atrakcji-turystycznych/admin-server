package uam.server.admin.spring.facade;

import uam.server.admin.data.impl.StudentAreaData;

import java.util.List;

public interface StudentAreaFacade {

    List<StudentAreaData> getAllStudentArea();

    List<StudentAreaData> getAreaByWebTrue();

}
