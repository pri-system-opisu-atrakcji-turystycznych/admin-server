package uam.server.admin.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.admin.data.impl.UserData;
import uam.server.admin.model.UserModel;
import uam.server.admin.spring.converter.Converter;
import uam.server.admin.spring.facade.UserFacade;
import uam.server.admin.spring.service.UserService;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserFacadeImpl implements UserFacade {

    @Resource
    private UserService userService;
    @Resource
    private Converter<UserData, UserModel> userDataUserModelConverter;

    @Override
    public List<UserData> getAll() {
        return userService.findAllActiveUsers().stream()
                .map(u -> userDataUserModelConverter.convert(u))
                .collect(Collectors.toList());
    }

    @Override
    public void addUser(final UserData user) {
        userService.saveUser(new UserModel(null, user.getEmail(), user.getPassword(), null, null));
    }

    @Override
    public void deleteUser(final String email) {
        userService.deleteUser(email);
    }
}
