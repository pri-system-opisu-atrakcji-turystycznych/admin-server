package uam.server.admin.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.admin.data.impl.StudentAreaData;
import uam.server.admin.model.AreaModel;
import uam.server.admin.spring.converter.Converter;
import uam.server.admin.spring.facade.StudentAreaFacade;
import uam.server.admin.spring.service.AreaService;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentAreaFacadeImpl implements StudentAreaFacade {

    @Resource
    private AreaService areaService;
    @Resource
    private Converter<StudentAreaData, AreaModel> studentAreaDataAreaModelConverter;

    @Override
    public List<StudentAreaData> getAllStudentArea() {
        return areaService.getAll().stream()
                .map(a -> studentAreaDataAreaModelConverter.convert(a))
                .collect(Collectors.toList());
    }

    @Override
    public List<StudentAreaData> getAreaByWebTrue() {
        return areaService.getAreaByWebTrue().stream()
                .map(a -> studentAreaDataAreaModelConverter.convert(a))
                .collect(Collectors.toList());
    }
}
