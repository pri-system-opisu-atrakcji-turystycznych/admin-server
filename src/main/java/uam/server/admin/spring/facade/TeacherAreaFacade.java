package uam.server.admin.spring.facade;

import uam.server.admin.data.impl.TeacherAreaData;
import uam.server.admin.model.AreaModel;

import java.util.List;

public interface TeacherAreaFacade {

    List<TeacherAreaData> getAllTeacherArea();

    List<TeacherAreaData> getAreaByMobileTrue();

    AreaModel getTeacherAreaByName(final String name);

    AreaModel getTeacherAreaByUrl(final String url);

    void addArea(final TeacherAreaData area);

    void modifyArea(final TeacherAreaData area);

    void deleteArea(final Long id);

}
