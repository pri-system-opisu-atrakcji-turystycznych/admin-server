package uam.server.admin.spring.facade.impl;

import org.springframework.stereotype.Service;
import uam.server.admin.data.impl.TeacherAreaData;
import uam.server.admin.exception.AreaNotFoundException;
import uam.server.admin.model.AreaModel;
import uam.server.admin.spring.converter.Converter;
import uam.server.admin.spring.facade.TeacherAreaFacade;
import uam.server.admin.spring.service.AreaService;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherAreaFacadeImpl implements TeacherAreaFacade {

    @Resource
    private AreaService areaService;
    @Resource
    private Converter<TeacherAreaData, AreaModel> teacherAreaDataAreaModelConverter;

    @Override
    public List<TeacherAreaData> getAllTeacherArea() {
        return areaService.getAll().stream()
                .map(a -> teacherAreaDataAreaModelConverter.convert(a))
                .collect(Collectors.toList());
    }

    @Override
    public List<TeacherAreaData> getAreaByMobileTrue() {
        return areaService.getAreaByMobileTrue().stream()
                .map(a -> teacherAreaDataAreaModelConverter.convert(a))
                .collect(Collectors.toList());
    }

    @Override
    public AreaModel getTeacherAreaByName(String name) {
        return areaService.getAreaByName(name)
                .orElseThrow(() -> new AreaNotFoundException(name));
    }

    @Override
    public AreaModel getTeacherAreaByUrl(String url) {
        return areaService.getAreaByUrl(url)
                .orElseThrow(() -> new AreaNotFoundException(url));
    }

    @Override
    public void addArea(TeacherAreaData area) {
        areaService.addArea(area);
    }

    @Override
    public void modifyArea(TeacherAreaData area) {
        areaService.modifyArea(area);
    }

    @Override
    public void deleteArea(Long id) {
        areaService.deleteArea(id);
    }

}
