package uam.server.admin.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.admin.model.UserModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserModel, Long>, PagingAndSortingRepository<UserModel, Long> {

    List<UserModel> findAllByActiveIsIn(final Long active);

    Optional<UserModel> findByEmail(final String email);

}