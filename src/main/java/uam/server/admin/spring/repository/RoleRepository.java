package uam.server.admin.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.admin.model.RoleModel;

import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<RoleModel, Long>, PagingAndSortingRepository<RoleModel, Long> {

    Optional<RoleModel> findByName(final String name);

}