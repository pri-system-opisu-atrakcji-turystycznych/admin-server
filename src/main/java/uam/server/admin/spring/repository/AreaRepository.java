package uam.server.admin.spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import uam.server.admin.model.AreaModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface AreaRepository extends CrudRepository<AreaModel, Long>, PagingAndSortingRepository<AreaModel, Long> {

    Optional<AreaModel> findById(final Long id);

    Optional<AreaModel> findByName(final String name);

    Optional<AreaModel> findByUrl(final String url);

    List<AreaModel> findByMobileTrue();

    List<AreaModel> findByWebTrue();

}
