package uam.server.admin.spring.service.impl;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import uam.server.admin.exception.RoleNotFoundException;
import uam.server.admin.exception.UserNotFoundException;
import uam.server.admin.model.RoleModel;
import uam.server.admin.model.UserModel;
import uam.server.admin.spring.repository.RoleRepository;
import uam.server.admin.spring.repository.UserRepository;
import uam.server.admin.spring.service.UserService;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;
    @Resource
    private RoleRepository roleRepository;
    @Resource
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public List<UserModel> findAllActiveUsers() {
        return userRepository.findAllByActiveIsIn(1L);
    }

    public UserModel findUserByEmail(final String email) {
        return userRepository.findByEmail(email).orElseThrow(() -> new UserNotFoundException(email));
    }

    public void saveUser(final UserModel user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(1L);
        RoleModel userRoleModel = roleRepository.findByName("ADMIN").orElseThrow(() -> new RoleNotFoundException("ADMIN"));
        user.setRoles(new HashSet<>(Arrays.asList(userRoleModel)));
        userRepository.save(user);
    }

    @Override
    public void deleteUser(final String email) {
        UserModel user = findUserByEmail(email);
        user.setActive(0L);
        userRepository.save(user);
    }
}