package uam.server.admin.spring.service;

import uam.server.admin.data.impl.TeacherAreaData;
import uam.server.admin.model.AreaModel;

import java.util.List;
import java.util.Optional;

public interface AreaService {

    List<AreaModel> getAll();

    List<AreaModel> getAreaByMobileTrue();

    List<AreaModel> getAreaByWebTrue();

    Optional<AreaModel> getAreaByName(final String name);

    Optional<AreaModel> getAreaByUrl(final String url);

    void addArea(final TeacherAreaData area);

    void modifyArea(final TeacherAreaData area);

    void deleteArea(final Long id);

}
