package uam.server.admin.spring.service.impl;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import uam.server.admin.data.impl.StudentAreaData;
import uam.server.admin.data.impl.TeacherAreaData;
import uam.server.admin.exception.AreaNotFoundException;
import uam.server.admin.model.AreaModel;
import uam.server.admin.spring.converter.Converter;
import uam.server.admin.spring.repository.AreaRepository;
import uam.server.admin.spring.service.AreaService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class AreaServiceImpl implements AreaService {

    @Resource
    private AreaRepository areaRepository;
    @Resource
    private Converter<StudentAreaData, AreaModel> studentAreaDataAreaModelConverter;
    @Resource
    private Converter<TeacherAreaData, AreaModel> teacherAreaDataAreaModelConverter;

    @Override
    public List<AreaModel> getAll() {
        return Lists.newArrayList(areaRepository.findAll());
    }

    @Override
    public List<AreaModel> getAreaByMobileTrue() {
        return areaRepository.findByMobileTrue();
    }

    @Override
    public List<AreaModel> getAreaByWebTrue() {
        return areaRepository.findByWebTrue();
    }

    @Override
    public Optional<AreaModel> getAreaByName(final String name) {
        return areaRepository.findByName(name);
    }

    @Override
    public Optional<AreaModel> getAreaByUrl(final String url) {
        return areaRepository.findByUrl(url);
    }

    @Override
    public void addArea(TeacherAreaData area) {
        areaRepository.save(teacherAreaDataAreaModelConverter.inverseConvert(area));
    }

    @Override
    public void modifyArea(TeacherAreaData area) {
        areaRepository.save(teacherAreaDataAreaModelConverter.inverseConvert(area));
    }

    @Override
    public void deleteArea(Long id) {
        areaRepository.delete(areaRepository.findById(id).orElseThrow(() -> new AreaNotFoundException(id)));
    }
}
