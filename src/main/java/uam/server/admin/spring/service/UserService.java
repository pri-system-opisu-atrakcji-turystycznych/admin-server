package uam.server.admin.spring.service;

import uam.server.admin.model.UserModel;

import java.util.List;

public interface UserService {

    List<UserModel> findAllActiveUsers();

    UserModel findUserByEmail(final String email);

    void saveUser(final UserModel userModel);

    void deleteUser(final String email);
}