package uam.server.admin.constant;

public interface WebConstants {

    interface Mapping {
        String ROOT = "/";
        String LOGIN = ROOT + "login";
        String ADMIN = ROOT + "admin";
        String SERVER = ROOT + "server";
        String STATUS = ROOT + "status";
        String AREAS = ROOT + "areas";
        String MOBILE = ROOT + "mobile";
        String TEACHER = ROOT + "teacher";
        String STUDENT = ROOT + "student";
        String WEB = ROOT + "web";
        String AREA = ROOT + "area";
        String USER = ROOT + "user";
        String HOME = ROOT + "home";
        String AREA_ADD = AREA + ROOT + "add";
        String AREA_MODIFY = AREA + ROOT + "modify";
        String AREA_DELETE = AREA + ROOT + "delete";
        String USER_ADD = USER + ROOT + "add";
        String USER_DELETE = USER + ROOT + "delete";
        String NAME = AREA + ROOT + "name";
        String URL = AREA + ROOT + "url";
    }

    interface Views {
        String ADMIN_PAGE = "adminPage";
        String LOGIN_PAGE = "loginPage";
    }

    interface Attribute {
        String AREAS = "areas";
        String USERS = "users";
    }

}
