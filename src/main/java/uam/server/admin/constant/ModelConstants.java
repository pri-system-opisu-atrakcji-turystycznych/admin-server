package uam.server.admin.constant;

public interface ModelConstants {

    interface Area {
        String TABLE = "Area";
        String ID = "id";
        String NAME = "name";
        String URL = "url";
        String TEACHER_CODE = "teacherCode";
        String MOBILE = "mobile";
        String WEB = "web";
    }

    interface User {
        String TABLE = "User";
        String ID = "id";
        String EMAIL = "email";
        String PASSWORD = "password";
        String ACTIVE = "active";
        String USER_ROLES = "userRoles";
    }

    interface Role {
        String TABLE = "Role";
        String ID = "id";
        String NAME = "name";
    }

}

