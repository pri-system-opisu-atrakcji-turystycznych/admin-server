--
-- roles
--
INSERT INTO role VALUES (1,'ADMIN');
--
-- areas
--
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Luboń', 'http://150.254.78.178:9002/localserwer/', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Poznań', 'http://150.254.78.172:8081/localserwer/', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Stary rynek w Poznaniu', 'http://www.stary.poznan.pl', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Września', 'http://www.wr.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Wydział matematyki i informatyki UAM', 'http://www.wmi.uam.edu.pl', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Kołobrzeg', 'http://www.kol.pl', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Śrem', 'http://www.srem.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Kraków', 'http://www.krk.pl', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Środa Wielkopolska', 'http://www.sroda.wielkopolska.pl', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Gniezno', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Warszawa', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Gdańsk', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Gdynia', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Sopot', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Lublin', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Toruń', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Szczecin', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Olsztyn', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Rzeszów', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Białystok', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Kielce', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Częstochowa', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Gliwice', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Opole', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Zakopane', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Bytom', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, false, 'Zielona Góra', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, false, 'Sosnowiec', 'http://www.miasto.com', '0000', true);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Legnica', 'http://www.miasto.com', '0000', false);
INSERT INTO area (id, mobile, name, url, teacher_code, web) VALUES (default, true, 'Nowe Miasto', 'http://www.miasto.com', '0000', false);