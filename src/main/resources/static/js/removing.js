var APP = APP || {};

APP.removing = (function () {

    (function () {
    }());

    function confirmRemoving(buttonIdTemplate, checkboxIdTemplate, id) {
        if ($("#" + checkboxIdTemplate + id).prop("checked")) {
            $("#" + buttonIdTemplate + id).prop('disabled', false);
        } else {
            $("#" + buttonIdTemplate + id).prop('disabled', true);
        }
    }

    return {
        confirmRemoving: confirmRemoving
    }

})();