var APP = APP || {};

APP.menu = (function () {

    (function () {
        $('#areas').show();
        $('#users').hide();
    }());


    function setSection(section) {
        if (section === 'areas') {
            $('#areas').show();
            $('#users').hide();

        }
        if (section === 'users') {
            $('#areas').hide();
            $('#users').show();
        }
    }

    return {
        setSection: setSection
    }

})();