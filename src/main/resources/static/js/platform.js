var APP = APP || {};

APP.platform = (function () {

    (function () {
    }());

    function setPlatform(inputId) {
        if ($("#" + inputId).val() === 'true') {
            $("#" + inputId).val('false');
        } else {
            $("#" + inputId).val('true');
        }
    }

    return {
        setPlatform: setPlatform
    }

})();