package uam.server.admin.spring.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;
import uam.server.admin.Application;

import static org.junit.Assert.assertNotNull;

@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class AreaServiceImpl {

    @TestConfiguration
    static class AreaServiceImplTestContextConfiguration {
        @Bean
        @Primary
        public AreaService areaService() {
            return Mockito.mock(AreaService.class);
        }
    }

    @Autowired
    private AreaService areaService;

    @Test
    public void getAll() {
        assertNotNull(areaService.getAll());
    }

    @Test
    public void getAreaByMobileTrue() {
        assertNotNull(areaService.getAreaByMobileTrue());
    }

    @Test
    public void getAreaByWebTrue() {
        assertNotNull(areaService.getAreaByWebTrue());
    }

    @Test
    public void getAreaByName() {
        assertNotNull(areaService.getAreaByName("Luboń"));
    }

}
