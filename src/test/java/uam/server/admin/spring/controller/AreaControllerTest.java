package uam.server.admin.spring.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import uam.server.admin.constant.WebConstants.Mapping;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AreaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAllStudentArea() throws Exception {
        this.mockMvc.perform(get(Mapping.AREAS + Mapping.STUDENT).contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());

    }

    @Test
    public void getAllTeacherArea() throws Exception {
        this.mockMvc.perform(get(Mapping.AREAS + Mapping.TEACHER).contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getMobileAreas() throws Exception {
        this.mockMvc.perform(get(Mapping.AREAS + Mapping.MOBILE).contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getWebAreas() throws Exception {
        this.mockMvc.perform(get(Mapping.AREAS + Mapping.WEB).contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

}
