package uam.server.admin.spring.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import uam.server.admin.constant.WebConstants.Mapping;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AdminPageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getPage() throws Exception {
        this.mockMvc.perform(get(Mapping.ADMIN + Mapping.HOME).secure(true))
                .andExpect(status().is(302));
    }

    @Test
    public void addArea() throws Exception {
        this.mockMvc.perform(get(Mapping.ADMIN + Mapping.AREA_ADD).secure(true))
                .andExpect(status().is(302));

    }

    @Test
    public void modifyArea() throws Exception {
        this.mockMvc.perform(get(Mapping.ADMIN + Mapping.AREA_MODIFY).secure(true))
                .andExpect(status().is(302));
    }

    @Test
    public void deleteArea() throws Exception {
        this.mockMvc.perform(get(Mapping.ADMIN + Mapping.AREA_DELETE).secure(true))
                .andExpect(status().is(302));
    }

}
