# Admin-server

### Instalowanie
```
    mvn install
```
```
    cd target
```
```    
    java -jar artifact-1.0-SNAPSHOT.war
```


## API Endpoint
|Endpoints|Użycie|Parametry|
|---|---|---|
|```GET /areas/student```|Zwraca wszystkie obszary (ucznia).|---|
|```GET /areas/teacher```|Zwraca wszystkie obszary (nauczyciela).|---|
|```GET /areas/mobile```|Zwraca obszary (ucznia) wykorzystujące aplikacje mobilną.|---|
|```GET /areas/web```|Zwraca obszary (nauczyciela) wykorzystujące aplikacje webową.|---|
|```GET /areas/area/name?name={name}```|Zwraca obszar (ucznia) o podanej nazwy obszaru.|**{name}** - nazwa obszaru|
|```GET /areas/area/url?url={url}```|Zwraca obszar (ucznia) o podanynm adresie url.|**{url}** - adres url|
|```GET /server/status```|Zwraca "Work!" jeśli serwer działa poprawnie.|---|