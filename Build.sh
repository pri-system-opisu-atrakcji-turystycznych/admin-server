#!/bin/sh

mvn clean install

docker image build --force-rm --tag admin-serv .

docker run --name admin-server -d --rm -p 9001:8080 admin-serv

